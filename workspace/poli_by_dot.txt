set_global_flag("reduction", False);
{
	SCALE := 30;
	WIDTH := 10;
	HEIGHT := 10;
}


# BEGIN
window := graph_create("poli out derivatives", "650x650+600+0");

output("point\n");
input("x: ", px, Fraction);

output("How many derivatives do you know in this point: ");
input("", n, Number);

m := L[];
for(i := 0; i <= n; i := i + 1)
{
	output("f{}({}) = ", i, px);
	input("", temp, Fraction);
	append(m, temp);
}
py := index(m, 0);

function draw_grid(window, px, py)
{
	for (x := 0; x <= WIDTH; x := x + 1) {
		graph_line(window, x * SCALE, 0, x * SCALE, (2*HEIGHT + 1) * SCALE, "#f00");
		graph_text(window, x * SCALE, (2*HEIGHT + 1) * SCALE + 10, "{}" % (x + int(px) - WIDTH), "#f00", 10);
	}
	for (x := 0; x <= WIDTH; x := x + 1) {
		graph_line(window, (x + WIDTH + 1) * SCALE, 0, (x + WIDTH + 1) * SCALE, (2*HEIGHT + 1) * SCALE, "#f00");
		graph_text(window, (x + WIDTH + 1) * SCALE, (2*HEIGHT + 1) * SCALE + 10, "{}" % (x + int(px) + 1), "#f00", 10);
	}
	for (y := 0; y <= HEIGHT; y := y + 1) {
		graph_line(window, 0, y * SCALE, (2*WIDTH + 1) * SCALE, y * SCALE, "#f00");
		graph_text(window, 10, y * SCALE + 10, "{}" % (HEIGHT - y + int(py)), "#f00", 10);
	}
	for (y := 0; y <= HEIGHT; y := y + 1) {
		graph_line(window, 0, (y + HEIGHT + 1) * SCALE, (2*WIDTH + 1) * SCALE, (y + HEIGHT + 1) * SCALE, "#f00");
		graph_text(window, 10, (y + HEIGHT + 1) * SCALE + 10, "{}" % (int(py) - y - 1), "#f00", 10);
	}
}
draw_grid(window, px, py);


function draw_point(x, y, s, window, px, py) {
	x := int((x - frac(int(px)) + frac(WIDTH)) * frac(SCALE));
	y := int((frac(HEIGHT) - y + frac(int(py))) * frac(SCALE));
	graph_line(window, x - s, y - s, x + s + 1, y + s + 1, "black");
	graph_line(window, x - s, y + s, x + s + 1, y - s - 1, "black");
}

function plot(p, start, end, steps, window, color, px, py) {
	lastX := (start - frac(int(px)) + frac(WIDTH)) * frac(SCALE);
	lastY := (frac(HEIGHT) - substitute(p, start) + frac(int(py))) * frac(SCALE);

	range := (end - start) / frac(steps);
	for (x := 1; x <= steps; x := x + 1) {
		xx := frac(x) * range;
		graphX := (xx - frac(int(px)) + frac(WIDTH)) * frac(SCALE);
		graphY := (frac(HEIGHT) - substitute(p, xx) + frac(int(py))) * frac(SCALE);
		graph_line(window, lastX, lastY, graphX, graphY, color);
		lastX := graphX;
		lastY := graphY;
	}
}

steps := WIDTH*10;
draw_point(px, py, 3, window, px, py);
if (n > 0)
{
	b := index(m, 0) - index(m, 1)*px;
	k := index(m, 1);
	plot([[b k]], frac(int(px)) - frac(WIDTH), 2.*frac(int(px)) + 1., steps, window, "green", px, py);
}

poli := [[]];
fact := 1.;
poli_x := [[1]];
px := 0. - px;
mul := [[px 1]];
px := 0. - px;

for(i := 0; i <= n; i := i + 1)
{
	poli := poli + poli_x*(index(m, i)/fact);
	fact := fact * frac((i + 1));
	poli_x := poli_x * mul;
}
plot(poli, frac(int(px)) - frac(WIDTH), 2.*frac(int(px)) + 1., steps, window, "blue", px, py);

mas_wind := L[];
for(i := 1; i <= n; i := i + 1)
{
	poli := der_p_p(poli);
	temp_wind := graph_create("{} derivative" % i, "650x650+600+0");
	append(mas_wind, temp_wind);
	draw_grid(temp_wind, px, index(m, i));
	draw_point(px, index(m, i), 3, temp_wind, px, index(m, i));
	plot(poli, frac(int(px)) - frac(WIDTH), 2.*frac(int(px)) + 1., steps, temp_wind, "blue", px, index(m, i));
	if (i < n - 1)
	{
		b := index(m, i) - index(m, i + 1)*px;
		k := index(m, i + 1);
		plot([[b k]], frac(int(px)) - frac(WIDTH), 2.*frac(int(px)) + 1., steps, temp_wind, "green", px, index(m, i));
	}
}
