set_global_flag("reduction", False);

function sin(x, n) {
	result := frac(0);
	f := 1;
	xx := x;
	for (i := 1; i <= n; i := i + 1) {
		add := xx / frac(f);
		if (i % 2 != 0) {
			result := result + add;
		}
		else {
			result := result - add;
		}
		f := f * (2 * i) * (2 * i + 1);
		xx := xx * x * x;
	}
	return frac(result);
}

function cos(x, n) {
	result := frac(0);
	f := 1;
	xx := 1/1;
	for (i := 1; i <= n; i := i + 1) {
		add := xx / frac(f);
		if (i % 2 != 0) {
			result := result + add;
		}
		else {
			result := result - add;
		}
		f := f * (2 * i) * (2 * i - 1);
		xx := xx * x * x;
	}
	return frac(result);
}

pi := 355/113 / 2/1;
pi90 := pi / 90/1;
N := 4;


output("sin = [\n");
for (arg := 0/1; arg <= pi; arg := arg + pi90) {
	res := sin(arg, N);
	output("    Fraction(numerator={}, denominator={}), \n", numerator(res), denominator(res));
}
output("\n]\ncos = [\n");
for (arg := 0/1; arg <= pi; arg := arg + pi90) {
	res := cos(arg, N);
	output("    Fraction(numerator={}, denominator={}), \n", numerator(res), denominator(res));
}
output("\n]");
