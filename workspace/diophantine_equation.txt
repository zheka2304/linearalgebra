set_global_flag("reduction", False);

output("\nPut number of variables: ");
input("", n, NNumber);

dots := L[];
sign := L[];
for(i := 0; i < n; i := i + 1)
{
	output("Put a{}: ", (i + 1));
	input("", temp, Number);
	if (temp >= 0)
	{
		append(sign, 1);
		append(dots, temp);
	}
	else
	{
		append(sign, -1);
		append(dots, 0 - temp);
	}
}
output("Put c: ");
input("", c, Number);

output("\n{}*x1 ", index(dots, 0));
for(i := 1; i < n; i := i + 1)
{
	if (index(sign, i) > 0)
	{
		output("+ {}*x{} ", index(dots, i), i + 1);
	}
	else
	{
	if (index(sign, i) < 0)
	{
		output("- {}*x{} ", index(dots, i), i + 1);
	}
	}
}
output("= {}\n", c);

mas := L[];
for(i := 0; i < n; i := i + 1)
{
	temp := L[];
	for(j := 0; j < n; j := j + 1)
	{
		if (j == i)
		{
			append(temp, 1);
		}
		else
		{
			append(temp, 0);
		}
	}
	append(mas, temp);
}

function is_end_evkl(dots, n)
{
	count := 0;
	for(i := 0; i < n; i := i + 1)
	{
		if (index(dots, i) == 0)
		{
			count := count + 1;
		}	
	}
	if (count >= n - 1)
	{
		return False;
	}
	else
	{
		return True;
	}
}

function find(dots, n, begin)
{
	for (i := begin; i < n; i := i + 1)
	{
		if (index(dots, i) != 0)
		{
			return i;
		}
	}
	return n;
}

while(is_end_evkl(dots, n))
{
	ind1 := find(dots, n, 0);
	ind2 := find(dots, n, ind1 + 1);
	if (index(dots, ind1) < index(dots, ind2))
	{
		temp := ind1;
		ind1 := ind2;
		ind2 := temp;
	}
	num := index(dots, ind1) / index(dots, ind2);
	for(i := 0; i < n; i := i + 1)
	{
		index(index(mas, ind1), i, index(index(mas, ind1), i) - num * index(index(mas, ind2), i));
	}
	index(dots, ind1, index(dots, ind1) % index(dots, ind2));
}

ind1 := find(dots, n, 0);
if (c % index(dots, ind1) != 0)
{
	output("This equation can't be solve.\n");
}
else
{
del := c/index(dots, ind1);

for(i := 0; i < n; i := i + 1)
{
	output("\nx{} = {} ", i + 1, index(index(mas, ind1), i)*del * index(sign, i));
	for (j := 0; j < ind1; j := j + 1)
	{
		if (index(index(mas, j), i)*index(sign, i) > 0)
		{
			output("+ {}*k{} ", index(index(mas, j), i)*index(sign, i), j + 1);
		}
		else
		{
		if (index(index(mas, j), i)*index(sign, i) < 0)
		{
			output("- {}*k{} ", 0 - index(index(mas, j), i)*index(sign, i), j + 1);
		}
		}
	}
	for (j := ind1 + 1; j < n; j := j + 1)
	{
		if (index(index(mas, j), i)*index(sign, i) > 0)
		{
			output("+ {}*k{} ", index(index(mas, j), i)*index(sign, i), j);
		}
		else
		{
		if (index(index(mas, j), i)*index(sign, i) < 0)
		{
			output("- {}*k{} ", 0 -index(index(mas, j), i)*index(sign, i), j);
		}
		}
	}
}
output("\n");
}
