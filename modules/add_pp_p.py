# Made by Pavel Klyosov
# 13.03.2019 05:20

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def add_pp_p(a, b):
    result = Polynomial([0])
    i = max(len(a), len(b)) - 1
    while i >= 0:
        result[i] = a[i] + b[i]
        i -= 1
    return result


register_module("add_pp_p",
                add_pp_p, Polynomial, (Polynomial, Polynomial, ),
                """
                Returns sum of two polynomials
                """)
__all__ = []

