# Created by Dmitry Elizarov 11.03.2019
# import module registry
from .registry import *
# import structures like Number, Fraction, ...
from structures import *


def sub_zz_z(a, b):
    if a.negative != b.negative:
        result = Number(call_module("add_nn_n", NNumber(abs(a)), NNumber(abs(b))))
        result.negative = a.negative
    else:
        if abs(a) >= abs(b):
            result = Number(call_module("sub_nn_n", NNumber(abs(a)), NNumber(abs(b))))
            result.negative = a.negative
        else:
            result = Number(call_module("sub_nn_n", NNumber(abs(b)), NNumber(abs(a))))
            result.negative = not a.negative
    return result


register_module("sub_zz_z",
                sub_zz_z, Number, (Number, Number),
                """
                Subtract two integers
                """)

__all__ = []

