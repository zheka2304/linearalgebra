# Created by Helen 10.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def sub_ndn_n(b, a, c):
    e = call_module("mul_nd_n", a, c)
    return call_module("sub_nn_n", b, e)


register_module("sub_ndn_n", sub_ndn_n, NNumber, (NNumber, NNumber, NNumber, ), "Returns first number - digit * second number")

__all__ = []
