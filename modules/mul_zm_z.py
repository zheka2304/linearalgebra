# Created by Dmitry Elizarov  09.03.2019
# import module registry
from .registry import *
# import structures like Number, Fraction, ...
from structures import *


def mul_zm_z(a):
    if a == 0:
        return a
    if a != 0:
        return a.opposite()


register_module("mul_zm_z",
                mul_zm_z, Number, (Number, ),
                """
                Multiplies a integer by -1
                """)

__all__ = []

