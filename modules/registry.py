import copy

from parsers import Parsable, ParserException


class ModuleException(Exception):
    pass


class ErrorResult:
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class Module:
    def __init__(self, name, func, return_type, params, description):
        self.name = name
        self.func = func
        self.return_type = return_type
        self.params = params
        self.description = description

    def __call__(self, *args, **kwargs):
        if len(args) > len(self.params):
            args = args[:len(self.params)]
        args_copy = []
        for arg in args:
            arg.validate()
            args_copy.append(copy.copy(arg))
        result = self.func(*tuple(args_copy), **kwargs)
        if isinstance(result, ErrorResult):
            return result
        elif isinstance(result, Parsable):
            if result.validate():
                return copy.copy(result)
            else:
                return ErrorResult("failed to validate result: " + str(result))
        else:
            return ErrorResult("module returned non-parsable result: " + str(result))

    def can_apply_for(self, args):
        if len(args) != len(self.params):
            return False
        for i in range(len(self.params)):
            if not isinstance(args[i], self.params[i]):
                return False
        return True


# default options are stored here
_default_options = {
    "reduction": True
}


class ModuleRegistry:
    def __init__(self):
        self.modules = {}
        self.options = _default_options.copy()

    def reset_options(self):
        for key, val in _default_options.items():
            self.options[key] = val

    def get_option(self, name):
        if name not in self.options:
            raise NameError("no such option: " + str(name))
        return self.options[name]

    def set_option(self, name, value):
        if name not in self.options:
            raise NameError("no such option: " + str(name))
        self.options[name] = value

    # register("com_nn_d",
    #           com_nn_d, Number, (Number, Number),
    #           """ description """)
    def register(self, name, *args):
        if name in self.modules:
            module_list = self.modules[name]
        else:
            module_list = []
            self.modules[name] = module_list
        module_list.append(Module(name, *args))
        module_list.sort(key=lambda m: len(m.params), reverse=True)

    def get_va(self, name, args):
        if name in self.modules:
            for module in self.modules[name]:
                if module.can_apply_for(args):
                    return module
        return None

    def get(self, name, *args):
        return self.get_va(name, tuple(args))

    def invoke(self, name, *args):
        return self.get(name, *args)(*args)


# Global instance of module registry
_module_registry = ModuleRegistry()


def get_instance():
    return _module_registry


def register_module(*args):
    get_instance().register(*args)


def call_module(name, *args):
    module = get_instance().get(name, *args)
    if module is None:
        _args = []
        for arg in args:
            _args.append(type(arg).__name__ + ":" + str(arg))
        raise ParserException("failed to find module " + name + " for (" + ", ".join(_args) + ")")
    result = get_instance().invoke(name, *args)
    if isinstance(result, ErrorResult):
        raise ParserException("call module " + name + " finished with error result: " + str(result))
    return result


__all__ = ["ModuleException", "ErrorResult", "Module", "ModuleRegistry", "register_module", "call_module", "get_instance"]
