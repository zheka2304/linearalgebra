# Created by Helen 6.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def add_nn_n(a, b):
    for i in range(max(len(a), len(b))):
        a[i] += b[i]
        if a[i] > 9:
            a[i] -= 10
            a[i + 1] += 1
    return a


register_module("add_nn_n", add_nn_n, NNumber, (NNumber, NNumber, ), "Returns sum of two natural numbers")

__all__ = []
