# Created by Dmitry Elizarov 10.03.2019
# import module registry
from .registry import *
# import structures like Number, Fraction, ...
from structures import *


def int_q_b(a):
    top = int(a.numerator)
    bot = int(a.denominator)
    if top % bot == 0:
        return Boolean(True)
    else:
        return Boolean(False)


register_module("int_q_b",
                int_q_b, Boolean, (Fraction, ),
                """
                Check if a rational number is an integer
                """)

__all__ = []

