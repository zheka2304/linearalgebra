# Made by Dmitry Elizarov
# 07.03.2019 14:30

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def abs_z_n(a):
    a.negative = False
    return NNumber(a)


register_module("abs_z_n",
                abs_z_n, NNumber, (Number, ),
                """
                Return absolute value of a number
                """)

__all__ = []
