# Made py Pavel Klyosov 
# 21.03.2019 


# import module registry 
from .registry import *

# import structures 

from structures import *


def div_pp_p(a, b):
    result = Polynomial([])
    deg_a = call_module("deg_p_n", a)
    deg_b = call_module("deg_p_n", b)
    if deg_b == 0:
        return call_module("mul_pq_p", a, Fraction(1) / b[0])
    while deg_a >= deg_b:
        result[int(deg_a - deg_b)] = a[int(deg_a)] / b[int(deg_b)]
        a = a - call_module("mul_pxk_p", b, deg_a - deg_b) * result[int(deg_a - deg_b)]
        deg_a = call_module("deg_p_n", a)
    return result


register_module("div_pp_p",
                div_pp_p, Polynomial, (Polynomial, Polynomial, ),
                "Divides two polynomials, returns quotient")

__all__ = []

