# Made by Pavel Klyosov
# 12.03.2019 22:00

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def div_zz_z(a, b):
    if a.is_zero():
        return Number(0)
    if (a > 0 and b > 0) or (a < 0 and b < 0):
        return call_module("div_nn_n", NNumber(abs(a)), NNumber(abs(b)))
    else:
        z = call_module("sub_nn_n", NNumber(abs(a)), NNumber(1))
        x = call_module("div_nn_n", NNumber(abs(a)), NNumber(abs(b)))
        y = call_module("div_nn_n", NNumber(z), NNumber(abs(b)))
        if y == x:
            return -Number(call_module("add_1n_n", x))
        else:
            return -Number(x)


register_module("div_zz_z",
                div_zz_z, Number, (Number, Number),
                """
                Divides two integers, returns quotient
                """)

__all__ = []

