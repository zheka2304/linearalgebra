# Created by Dmitry Elizarov and Zubareva Elena 07.02.2019

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def add_1n_n(a):
    i = 0
    while a[i] == 9:
        a[i] = 0
        i += 1
    a[i] += 1
    return a


register_module("add_1n_n", add_1n_n, NNumber, (NNumber, ), "add 1 to natural number")

__all__ = []
