# Made py Pavel Klyosov
# 13.03.2019


# import module registry
from .registry import *

# import structures

from structures import *


def mul_pxk_p(a, k):
    l = len(a)
    b = Polynomial([])
    for i in range(0, int(l + k)):
        b[i] = Fraction(numerator=0)
    for i in range(0, l):
        b[(int(i + k))] = a[i]
    return b


register_module("mul_pxk_p",
                mul_pxk_p, Polynomial, (Polynomial, Number, ),
                "Returns a polynomial multiplied by x^k")

__all__ = []

