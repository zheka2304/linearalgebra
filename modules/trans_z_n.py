# Created by Helen 11.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def trans_z_n(a):
    b = NNumber(a)
    return b


register_module("trans_z_n", trans_z_n, NNumber, (Number, ), "Converts integer to natural")

__all__ = []


