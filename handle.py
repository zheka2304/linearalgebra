class WindowHandle:
    def destroyed(self):
        raise NotImplementedError()


class EnvironmentHandle:
    # events:
    #   clear
    #   clear-env
    #   run
    #   run-verbose
    #   terminate
    #   help
    #   open
    #   save
    #   save-as
    #   exit
    def add_event(self, name, callback, **kwargs):
        raise NotImplementedError()

    # methods:
    #   toggle_fullscreen
    #   ask_open_file
    #   ask_save_file
    #   get_source_code
    #   set_source_code
    #   destroy_children
    #   open_help
    #   exit
    def invoke_method(self, name, *args, **kwargs):
        raise NotImplementedError()

    def get_shell(self):
        raise NotImplementedError()

    def protocol(self, name, callback):
        raise NotImplementedError()

    def mainloop(self):
        raise NotImplementedError()

