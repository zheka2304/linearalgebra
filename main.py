from environment import *
from interface import *

if __name__ == "__main__":
    main_environment = Environment()
    main_environment.attach_handle(Window())
    main_environment.mainloop()

