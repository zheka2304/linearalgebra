

class ParserSpan:
    def __init__(self, source, start=None, end=None, parent=None):
        assert source is not None

        if start is None:
            start = 0
        if end is None:
            end = len(source)

        assert start >= 0
        assert end <= len(source)
        assert end >= start
        self.source = source
        self.start = self._start = start
        self.end = end
        self.parent = parent

    def rewind(self):
        self.start = self._start

    def pos(self, pos=None):
        if pos is None:
            return self.start
        else:
            self.start = pos

    def __iadd__(self, x):
        self.start += x
        return self

    def __isub__(self, x):
        self.start -= x
        return self

    def __repr__(self):
        return self.source[:self.start] + "[[" + self.source[self.start:self.end] + "]]" + self.source[self.end:]

    def __str__(self):
        return self.source[self.start:self.end]

    def __len__(self):
        return self.end - self.start

    def __getitem__(self, key):
        key += self.start
        if key >= self.end:
            raise IndexError()
        return self.source[key]

    def __setitem__(self, key, val):
        key += self.start
        if key >= self.end:
            raise IndexError()
        self.source[key] = val

    def __iter__(self):
        return self.__str__().__iter__()

    def span(self, start=None, end=None):
        if start is None:
            start = 0
        if end is None:
            end = len(self)
        start += self.start
        end += self.start
        if self.start <= start <= end <= self.end:
            return ParserSpan(self.source, start=start, end=end, parent=self)
        else:
            raise IndexError("({}, {}) not included in ({}, {}) ".format(start, end, self.start, self.end))

    def trim(self):
        start = 0
        end = len(self)
        while end > start:
            if self[start].isspace():
                start += 1
            elif self[end - 1].isspace():
                end -= 1
            else:
                break

        return self.span(start, end)

    def slice(self, start=None, end=None):
        return self.span(start=start, end=end)


__all__ = ["ParserSpan"]
