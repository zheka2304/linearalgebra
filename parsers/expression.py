from .variable import *
from .value import *
from .exception import *


# Contains max size of operator string
_max_operator_length = 4


def get_operator_priority(operator):
    if operator in ["^", "**"]:
        return 2
    if operator in ["*", "/", "%"]:
        return 3
    if operator in ["+", "-"]:
        return 4
    if operator in ["==", ">", "<", ">=", "<=", "!="]:
        return 10
    if operator in ["&&", "||", "-->"]:
        return 11
    if operator in [":="]:
        return 15
    return -1


def is_operator(token):
    return get_operator_priority(token) != -1 or token in ["(", ")", ","]


def is_function(token):
    return isinstance(token, str) and not is_operator(token)


class Expression:
    def __init__(self, string=None, tokens=None, parsed=None, parser=None):
        self.parser = parser
        self.string = string
        self.tokens = tokens
        self.parsed = parsed

    def __str__(self):
        if self.tokens is not None:
            tokens = []
            for token in self.tokens:
                tokens.append(str(token))
            return " ".join(tokens)
        else:
            return "<not-parsed>"

    def __repr__(self):
        if self.tokens is not None:
            tokens = []
            for token in self.tokens:
                tokens.append(str(token))
            return "[Expression tokens='" + " ".join(tokens) + "']"
        else:
            return "[Expression span='" + str(self.string) + "']"

    def parsed_to_string(self):
        if self.parsed is not None:
            tokens = []
            for token in self.parsed:
                tokens.append(str(token))
            return " ".join(tokens)
        else:
            return "<not-parsed>"


class ExpressionParser:
    def __init__(self, modules, values):
        self.modules = modules
        self.values = values

    def for_scope(self, scope):
        return ExpressionParser(self.modules, scope)

    def parse(self, span):
        span = span.slice()
        string = str(span)
        tokens = []
        while len(span) > 0:
            val = self.values.parse(span, variables=True)
            if val is not None:
                tokens.append(val)
            else:
                name = ""
                while len(span) > 0 and not span[0].isspace():
                    flag = False
                    for l in reversed(range(1, min(len(span), _max_operator_length) + 1)):
                        op = str(span)[:l]
                        if is_operator(op):
                            flag = True
                            break
                    if flag:
                        break
                    name += span[0]
                    span += 1
                if len(name) > 0:
                    tokens.append(name)

            while len(span) > 0 and span[0].isspace():
                span += 1
            flag = True
            while flag and len(span) > 0:
                flag = False
                for l in reversed(range(1, min(len(span), _max_operator_length) + 1)):
                    op = str(span)[:l]
                    if is_operator(op):
                        tokens.append(op)
                        span += l
                        flag = True
                        break

        flag = True
        while flag:
            flag = False
            for i in range(len(tokens)):
                # resolve opposite values
                if i > 0 and (i < 2 or is_operator(tokens[i - 2]) and tokens[i - 2] != ")") and tokens[i - 1] == "-" and isinstance(tokens[i], Parsable):
                    tokens[i] = tokens[i].opposite()
                    tokens = tokens[:i - 1] + tokens[i:]
                    flag = True
                    break
                # resolve variable assignment
                if i > 0 and isinstance(tokens[i - 1], str) and not is_operator(tokens[i - 1]) and tokens[i] == ":=":
                    name = tokens[i - 1]
                    if is_valid_variable_name(name):
                        tokens[i - 1] = self.values.get_or_create_variable(name)
                        flag = True
                        break
                # resolve variable values
                if isinstance(tokens[i], Variable) and (i == len(tokens) - 1 or tokens[i + 1] != ":="):
                    tokens[i] = tokens[i].value
                    flag = True
                    break

        return self._expression(string, tokens)

    def _expression(self, string, tokens):
        queue = []
        operators = []
        for token in tokens:
            op1 = get_operator_priority(token)
            if isinstance(token, Parsable):
                queue.append(token)
            elif is_function(token):
                operators.append(token)
            elif token == ",":
                while len(operators) > 0 and operators[-1] != "(":
                    queue.append(operators.pop())
                if len(operators) == 0:
                    raise ParserException("mission ( or ,")
            elif op1 != -1:
                while len(operators) > 0 and operators[-1] != "(":
                    op2 = get_operator_priority(operators[-1])
                    if op1 >= op2 != -1:
                        queue.append(operators.pop())
                    else:
                        break
                operators.append(token)
            elif token == "(":
                operators.append(token)
            elif token == ")":
                while len(operators) > 0 and operators[-1] != "(":
                    queue.append(operators.pop())
                if len(operators) == 0:
                    raise ParserException("missing (")
                operators.pop()
                if len(operators) > 0 and is_function(operators[-1]):
                    queue.append(operators.pop())

        while len(operators) > 0:
            if operators[-1] == "(":
                raise ParserException("missing (")
            queue.append(operators.pop())
        return Expression(string=string, tokens=tokens[:], parsed=queue, parser=self)


__all__ = ["ExpressionParser"]
