from .exception import *
from .value import *


# TODO: add module associations to each operator in correct order
operator_associations = {
    "+": [
        "add_nn_n",
        "add_zz_z",
        "add_qq_q",
        "add_pp_p",
        "str_add",
        "arr_concat"
    ],
    "-": [
        "sub_zz_z",
        "sub_qq_q",
        "sub_pp_p",
    ],
    "*": [
        "mul_nn_n",
        "mul_zz_z",
        "mul_qq_q",
        "mul_pp_p",
        "mul_qp_p",
        "mul_pq_p"
    ],
    "/": [
        "div_nn_n",
        "div_zz_z",
        "div_qq_q",
        "div_pp_p",
    ],
    "%": [
        "mod_nn_n",
        "mod_zz_z",
        "mod_pp_p",
        "str_format",
    ],
    "^": [

    ],
    "**": [

    ],
    "==": [
        "logic_bb_eq",
        "com_zz_eq",
        "com_qq_eq",
        "com_pp_eq",
        "com_arr_eq"
    ],
    "!=": [
        "logic_bb_xor",
        "com_zz_neq",
        "com_qq_neq",
        "com_pp_neq",
        "com_arr_neq"
    ],
    "<": [
        "com_zz_lt",
        "com_qq_lt",
    ],
    ">": [
        "com_zz_gt",
        "com_qq_gt",
    ],
    "<=": [
        "com_zz_le",
        "com_qq_le",
    ],
    ">=": [
        "com_zz_ge",
        "com_qq_ge",
    ],
    "&&": [
        "logic_bb_and"
    ],
    "||": [
        "logic_bb_or"
    ],
    "-->": [
        "logic_bb_impl"
    ],
    ":=": [
        "__assign_variable"
    ]
}


class FunctionWrap:
    def __init__(self, caller, params):
        self.caller = caller
        self.params = params

    def __call__(self, *args, **kwargs):
        return self.caller(*args, **kwargs)


class StackInterpreter:
    def __init__(self, modules):
        self.modules = modules
        self.functions = {}

    def add_function(self, name, caller, params, no_override=False):
        if no_override and name in self.functions:
            raise ParserException("duplicate function definition: " + name)
        self.functions[name] = FunctionWrap(caller, params)

    def find_module(self, name, stack):
        if name in self.functions:
            return self.functions[name]
        for l in reversed(range(1, len(stack) + 1)):
            module = self.modules.get_va(name, stack[-l:])
            if module is not None:
                return module

    def get_association(self, token, stack):
        if token in operator_associations:
            for name in operator_associations[token]:
                module = self.find_module(name, stack)
                if module is not None:
                    return module

    # inputs Expression instance and interprets it as expression in inverse polish notation using module registries and value parser
    # expression array can be like [Fraction(1/2), Fraction(1/3), "module", "another_module", ...]
    def interpret(self, expression):
        stack = []
        for token in expression.parsed:
            if isinstance(token, str):
                module = self.find_module(token, stack)
                if module is None:
                    module = self.get_association(token, stack)
                
                if module:
                    args = []
                    if len(stack) < len(module.params):
                        raise ParserException("not enough params for to call " + token)
                    for i in range(len(module.params)):
                        args.insert(0, stack.pop())
                    result = module(*tuple(args))
                    if not isinstance(result, Parsable):
                        raise ParserException("module " + token + " exit with error result: " + str(result))
                    stack.append(result)
                else:
                    _stack = []
                    for arg in stack:
                        _stack.append(type(arg).__name__ + ":" + str(arg))
                    raise ParserException("failed to find module " + token + " for (" + ", ".join(_stack) + ")")
            else:
                stack.append(token)
        if len(stack) == 1:
            return stack[0]
        elif len(stack) == 0:
            raise ParserException("failed to interpret " + str(expression) + ", result stack is empty")
        elif len(stack) > 1:
            raise ParserException("failed to interpret " + str(expression) + ", result stack contains too many values: ")


__all__ = ["StackInterpreter", "operator_associations"]
