class Parsable:
    def parse(self, parser, span):
        raise NotImplementedError()

    def validate(self):
        raise NotImplementedError()


class Variable(Parsable):
    def __init__(self, values, name, val):
        self.values = values
        self.name = name
        self.value = val

    def __copy__(self):
        return self

    def __str__(self):
        s = "var:" + self.name
        if self.value is not None:
            s += ":" + str(self.value)
        return s

    def parse(self, parser, span):
        return False

    def validate(self):
        return True

    def assign(self, value):
        self.value = value
        self.values.assign(self)

    def opposite(self):
        return self.value.opposite()

