import re

from .span import *
from .exception import *


class InterpretationException(Exception):
    def __init__(self, message, statement=None, cause=None):
        super().__init__(message)
        self.statement = statement
        self.cause = cause

    def get_stack(self, indices=False, prefix=""):
        result = ""
        index = 0
        exception = self
        while exception is not None:
            if isinstance(exception, InterpretationException):
                result += prefix
                if indices:
                    result += "[#" + str(index + 1) + "] "
                result += repr(exception.statement) + "\n"
                exception = exception.cause
            else:
                exception = None
            index += 1
        return result


#
#


_statements = []


def _register_statement(regex, statement, priority=0):
    _statements.append((regex, statement, priority))
    _statements.sort(key=lambda x: x[2], reverse=True)


def _parse_statement(span, body):
    span = span.trim()
    for regex, clazz, priority in _statements:
        if re.match(regex, str(span)):
            statement = clazz(span.slice(), body=body)
            if statement.parse():
                return statement
            else:
                raise ParserException("failed to parse statement '" + str(span) + "'")


#
#


class ProgramStatement:
    def __init__(self, span, body=None, parent=None):
        self.span = span
        self.body = body
        self.parent = parent
        self.scope = None

    def parse(self):
        return True

    def __repr__(self):
        return str(self.span)

    def interpret_default(self, program, body, index, io, interpret_body=True):
        flag = True
        if interpret_body and self.body:
            flag = program.interpret_statement_array(self.body, caller=self, io=io)
        if flag and index < len(body) - 1:
            return body[index + 1]
        else:
            return None

    # interprets statement and returns next statement to interpret
    # - program - caller program
    # - parent - caller statement
    # - body - body array
    # - index - index of statement in body array
    # - flags - dictionary of shared properties for caller statement
    # return types:
    # - statement instance - just interpret it
    # - None - exit from body
    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        program.interpret_expression(self.span, statement=self)
        return self.interpret_default(program, body, index, io)


_register_statement(".*", ProgramStatement, priority=-1)


class FunctionResultException(Exception):
    def __init__(self, result):
        super().__init__("return outside function body")
        self.result = result


class ProgramStatementFunction(ProgramStatement):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = None
        self.args = []

    def parse(self):
        s = str(self.span)
        match = re.fullmatch("^function\s+(?P<name>[A-Za-z_][A-Za-z0-9_]*?)\s*\((?P<args>[^\(\)\{\}]*)\)$", s)
        if not match:
            return False
        self.args = []
        self.name = match.group("name")
        raw_args = match.group("args").split(",")
        for arg in raw_args:
            arg = arg.strip()
            if len(arg) == 0:
                if len(raw_args) > 1:
                    return False
                continue
            if re.fullmatch("[A-Za-z_][A-Za-z0-9_]*", arg):
                self.args.append(arg)
            else:
                raise ParserException("invalid function parameter name " + arg)
        return True

    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        def _caller(*args):
            subroutine = program.subroutine([])
            if len(args) != len(self.args):
                raise InterpretationException("invalid parameters for function {} expected {} got {}".format(self.name, len(self.args), len(args)))
            values = subroutine.values
            for i in range(len(self.args)):
                values.redefine(self.args[i], args[i])
            try:
                subroutine.interpret_statement_array(self.body, caller=self, io=io)
            except FunctionResultException as e:
                return e.result
            from structures import Boolean
            return Boolean(False)
        program.interpreter.add_function(self.name, _caller, self.args, no_override=True)
        return self.interpret_default(program, body, index, io, interpret_body=False)


_register_statement("^function\s+.*$", ProgramStatementFunction)


class ProgramStatementReturn(ProgramStatement):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.expression = None

    def parse(self):
        s = str(self.span)
        match = re.fullmatch("^return(?P<result>(\s+[^\{\}]*)?)$", s)
        if not match:
            return False
        self.expression = ParserSpan(match.group("result").strip())
        return True

    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        from structures import Boolean
        result = Boolean(False)
        if len(self.expression) > 0:
            result = program.interpret_expression(self.expression, statement=self)
        raise FunctionResultException(result=result)


_register_statement("^return\s+.*$", ProgramStatementReturn)


class ProgramStatementIf(ProgramStatement):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.condition = None

    def parse(self):
        self.condition = self.span.slice()
        while self.condition[0] != "(":
            self.condition += 1
        return self.body is not None

    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        result = program.interpret_expression(self.condition, statement=self)
        if type(result).__name__ == "Boolean":
            next_statement = body[index + 1] if index < len(body) - 1 else None
            if result:
                body = self.body
            else:
                if isinstance(next_statement, ProgramStatementElse):
                    body = next_statement.body
                else:
                    body = None
            complete = body is None or program.interpret_statement_array(body, caller=self, io=io)
            if complete:
                return next_statement
            else:
                return None
        else:
            raise InterpretationException("invalid (not Boolean) type of condition " + type(result).__name__ + ":" + str(result), statement=self)


class ProgramStatementElse(ProgramStatement):
    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        if index < len(body) - 1:
            return body[index + 1]
        else:
            return None


_register_statement("^if[\s]*\([^\{\};]*\)$", ProgramStatementIf)
_register_statement("^else$", ProgramStatementElse)


class ProgramStatementInput(ProgramStatement):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.variable_type = None
        self.variable_name = None
        self.prompt = ""

    def parse(self):
        s = str(self.span)
        if not re.fullmatch("^input\s*\(\s*\"[^\"]*\"\s*,\s*[A-Za-z_][A-Za-z0-9_]*\s*,\s*[A-Za-z]+\s*\)$", s):
            return False

        params = re.search("\(.*\)", s).group()[1:-1].split(",")
        self.prompt = params[0].strip()[1:-1]
        self.variable_name = params[1].strip()

        from pydoc import locate
        self.variable_type = locate("structures." + params[2].strip())
        return True

    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        val = io.read(self.prompt)
        while True:
            if program.terminated:
                return None
            parsed = program.values.parse(ParserSpan(val))
            if parsed is None:
                val = io.read("failed to parse '" + val + "', input again: ")
            elif not isinstance(parsed, self.variable_type):
                val = io.read("invalid value type, expected " + self.variable_type.__name__ + " got " + type(parsed).__name__ + ": ")
            else:
                break
        program.values.get_or_create_variable(self.variable_name).assign(parsed)
        return self.interpret_default(program, body, index, io)


_register_statement("^input\\s*\(.*\)$", ProgramStatementInput)


class ProgramStatementOutput(ProgramStatement):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pattern = ""
        self.values = []

    def _add_value_expression(self, val):
        s = ParserSpan(val.strip())
        if len(s) > 0:
            self.values.append(s)
        else:
            raise ParserException("failed to parse '" + str(self.span) + "', empty parameter expression found")

    def parse(self):
        s = str(self.span)
        if not re.fullmatch("^output\\s*\(\\s*\"[^\"]*\"\\s*\)$", s) and not re.fullmatch("^output\\s*\(\\s*\"[^\"]*\"\\s*,[^\\{\\};\"]*\)$", s):
            return False
        quotes = re.search("\\\"[^\\\"]*\\\"", s)
        quotes_and_comma = re.search("\\\"[^\\\"]*\"\\s*,", s)
        if quotes is None:
            return False
        self.pattern = quotes.group()[1:-1].replace("\\n", "\n")
        self.values = []
        if quotes_and_comma is None:
            return True
        values = s[quotes_and_comma.span()[1]:-1]

        # split parameter expressions
        braces = 0
        current = ""
        for i in range(len(values)):
            c = values[i]
            if braces == 0 and c == ",":
                self._add_value_expression(current)
                current = ""
            else:
                current += c
                if c == "(":
                    braces += 1
                if c == ")":
                    braces -= 1
        if len(current) > 0:
            self._add_value_expression(current)
        return True

    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        values = []
        for exp in self.values:
            values.append(program.interpret_expression(exp, statement=self))
        try:
            io.write(self.pattern.format(*tuple(values)), newline=False)
        except Exception:
            raise InterpretationException("failed to output: invalid format", statement=self)
        return self.interpret_default(program, body, index, io)


_register_statement("^output\\s*\(.*\)$", ProgramStatementOutput)


class ProgramStatementWhile(ProgramStatement):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.condition = None

    def parse(self):
        self.condition = self.span.slice()
        while self.condition[0] != "(":
            self.condition += 1
        return self.body is not None

    def _check_flags(self, flags, *args):
        for flag in args:
            if flag in flags and flags[flag]:
                return True
        return False

    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        if self._check_flags(flags, "break"):
            flags["break"] = False
            return self.interpret_default(program, body, index, io, interpret_body=False)
        result = program.interpret_expression(ParserSpan(str(self.condition)), statement=self)
        if type(result).__name__ == "Boolean":
            flag = True
            if bool(result):
                if program.interpret_statement_array(self.body, caller=self, io=io):
                    return self
                else:
                    flag = False
            flags["for-loop"] = False
            flags["continued"] = False
            if flag and index < len(body) - 1:
                return body[index + 1]
            else:
                return None
        else:
            raise InterpretationException("invalid (not Boolean) type of condition " + type(result).__name__ + ":" + str(result), statement=self)


_register_statement("^while[\s]*\([^\{\};]*\)$", ProgramStatementWhile)


class ProgramStatementFor(ProgramStatementWhile):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.step = None
        self.start = None

    def parse(self):
        s = str(self.span)
        params = re.search("\(.*\)", s).group()[1:-1].split(";")
        self.start = ParserSpan(params[0].strip())
        self.condition = ParserSpan(params[1].strip())
        self.step = ParserSpan(params[2].strip())
        return True

    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        if self._check_flags(flags, "break"):
            flags["break"] = False
            return self.interpret_default(program, body, index, io, interpret_body=False)
        if not self._check_flags(flags, "for-loop", "continued"):
            if len(self.start) > 0:
                program.interpret_expression(self.start, statement=self)
        if self._check_flags(flags, "continued"):
            if len(self.step) > 0:
                program.interpret_expression(self.step, statement=self)

        result = program.interpret_expression(ParserSpan(str(self.condition)), statement=self)
        if type(result).__name__ == "Boolean":
            flag = True
            if bool(result):
                if program.interpret_statement_array(self.body, caller=self, io=io):
                    flags["for-loop"] = True
                    flags["continued"] = True
                    return self
                else:
                    flag = False
            flags["for-loop"] = False
            flags["continued"] = False
            if flag and index < len(body) - 1:
                return body[index + 1]
            else:
                return None
        else:
            raise InterpretationException("invalid (not Boolean) type of condition " + type(result).__name__ + ":" + str(result), statement=self)


_register_statement("^for\s*\([^\{\};]*\;[^\{\};]*\;[^\{\};]*\)$", ProgramStatementFor)


class ProgramStatementBreak(ProgramStatement):
    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        loop = self
        while not isinstance(loop, ProgramStatementWhile):
            loop = loop.parent
            if loop is None:
                raise InterpretationException("continue used outside the loop", statement=self)
        flags["break"] = True
        flags["for-loop"] = False
        flags["continued"] = False
        return loop


class ProgramStatementContinue(ProgramStatement):
    def interpret(self, program, parent=None, body=None, index=-1, flags=None, io=None):
        loop = self
        while not isinstance(loop, ProgramStatementWhile):
            loop = loop.parent
            if loop is None:
                raise InterpretationException("continue used outside the loop", statement=self)
        flags["continued"] = True
        return loop


_register_statement("^break$", ProgramStatementBreak)
_register_statement("^continue$", ProgramStatementContinue)


#
#


class Program:
    def __init__(self, interpreter, values, parser, statements, parent=None):
        self.modules = interpreter.modules
        self.interpreter = interpreter
        self.values = values
        self.parser = parser
        self.statements = statements
        self.terminated = False
        self.parent = parent
        self.children = []

    def _check_terminated(self, statement=None):
        if self.terminated:
            raise InterpretationException("Program was terminated", statement=statement)

    def subroutine(self, statements):
        scope = self.values.scope(deep=True)
        parser = self.parser.for_scope(scope)
        sub = Program(self.interpreter, scope, parser, statements, parent=self)
        self.children.append(sub)
        return sub

    def interpret_expression(self, span, statement=None):
        # noinspection PyBroadException
        exp = None
        try:
            exp = self.parser.parse(span)
            result = self.interpreter.interpret(exp)
            if type(result).__name__ == "ErrorResult":
                raise InterpretationException("failed to interpret expression '" + exp.parsed_to_string() + "'", statement=statement)
            return result
        except InterpretationException as e:
            raise InterpretationException(str(e), cause=e, statement=statement)
        except Exception as e:
            import traceback
            print(traceback.format_exc())
            if exp is not None:
                print("parsed expression:", exp.parsed_to_string())
            raise InterpretationException(str(e) + " while interpreting " + str(span), cause=e, statement=statement)

    # interprets statement array
    # returns if it is finished with last statement
    def interpret_statement_array(self, statements, caller=None, io=None, flags=None):
        if flags is None:
            flags = {}
        if len(statements) > 0:
            statement = statements[0]
            while statement is not None:
                self._check_terminated(statement=statement)
                index = -1
                inside = False
                body = statements
                if statement in statements:
                    index = statements.index(statement)
                    inside = True
                elif statement in statement.scope:
                    body = statement.scope
                    index = statement.scope.index(statement)
                # interpret
                next_statement = statement.interpret(self, parent=caller, body=body, index=index, flags=flags, io=io)
                if next_statement is None and inside and index == len(statements) - 1:
                    return True
                statement = next_statement
            return False
        else:
            return True

    def terminate(self):
        self.terminated = True
        for child in self.children:
            child.terminate()

    def interpret(self, **kwargs):
        self.terminated = False
        self.modules.reset_options()
        self.interpret_statement_array(self.statements, **kwargs)

    def _repr_statement_arr(self, arr, prefix=""):
        s = ""
        for statement in arr:
            s += prefix + repr(statement)
            if statement.body is not None:
                s += "{\n" + self._repr_statement_arr(statement.body, prefix="  " + prefix) + prefix + "}"
            s += "\n"
        return s

    def __repr__(self):
        return "Program:\n" + self._repr_statement_arr(self.statements, prefix="  ")


#
#


class ParserNode:
    def __init__(self, parser, span):
        self.parser = parser
        self.span = span
        self.children = []


class ProgramParser:
    def __init__(self, interpreter, values, parser):
        self.interpreter = interpreter
        self.values = values
        self.parser = parser

    def parse_source(self, span):
        origin = span.slice()
        braces = 1
        brackets = 0
        statements = []
        pos = 0
        while len(span) > 0 and braces > 0:
            # resolve ; inside ()
            if span[0] == "(":
                brackets += 1
            if span[0] == ")":
                brackets -= 1
            # resolve strings
            if span[0] in ["'", '"']:
                quote = span[0]
                span += 1
                while len(span) > 0 and span[0] != quote:
                    span += 1
                if len(span) == 0:
                    raise ParserException("unterminated quote")
            # resolve statements
            if span[0] in ["{", "}"] or (span[0] == ";" and brackets == 0):
                statement = origin.slice(pos, span.pos() - origin.pos()).trim()
                if len(statement) > 0:
                    statements.append(statement)
                if span[0] == "{":
                    braces += 1
                    span += 1
                    statements.append(self.parse_source(span))
                    span -= 1
                if span[0] == "}":
                    braces -= 1
                pos = span.pos() - origin.pos() + 1
            span += 1
        statement = origin.slice(pos, span.pos() - origin.pos()).trim()
        if len(statement) > 0:
            statements.append(statement)
        # if braces > 0:
        #     raise ParserException("failed to parse '" + str(origin) + "': missing x" + str(braces) + " }")
        return statements

    def parse_statements(self, source):
        source = source[:]
        # recursively parse bodies
        for index in range(len(source)):
            statement = source[index]
            if isinstance(statement, list):
                source[index] = self.parse_statements(statement)
        # attach bodies to statements
        flag = True
        while flag:
            flag = False
            for index in range(len(source)):
                statement = source[index]
                if isinstance(statement, ParserSpan):
                    if index < len(source) - 1:
                        body = source[index + 1]
                        if isinstance(body, list):
                            source[index] = _parse_statement(statement, body=body)
                            for statement in body:
                                statement.parent = source[index]
                            source = source[:index + 1] + source[index + 2:]
                            flag = True
                            break
                    source[index] = _parse_statement(statement, body=None)
        # resolve unattached bodies
        flag = True
        while flag:
            flag = False
            for index in range(len(source)):
                statement = source[index]
                if isinstance(statement, list):
                    source = source[:index] + statement + source[index + 1:]
                    flag = True
                    break
        # attach scope
        for statement in source:
            statement.scope = source
        return source

    def preprocess(self, span):
        source = str(span)
        source = re.sub('\\"\\"\\"((?!\\"\\"\\")[\s\S])*\\"\\"\\"', " ", source)
        source = re.sub("\\'\\'\\'((?!\\'\\'\\')[\s\S])*\\'\\'\\'", " ", source)
        lines = source.split("\n")
        for i in range(len(lines)):
            line = lines[i]
            j = 0
            while j < len(line):
                if line[j] in ("'", '"'):
                    quote = line[j]
                    j += 1
                    while j < len(line) and line[j] != quote:
                        j += 1
                if line[j] == "#":
                    lines[i] = line[:j]
                    break
                j += 1
        return ParserSpan(" ".join(lines))

    def parse(self, span):
        source = self.parse_source(self.preprocess(span))
        statements = self.parse_statements(source)
        return Program(interpreter=self.interpreter, values=self.values, parser=self.parser, statements=statements)

