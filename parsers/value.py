import re

from .variable import *


def is_valid_variable_name(name):
    for i in range(len(name)):
        if i == 0 and not (name[i].isalpha() or name[i] == "_"):
            return False
        if not(name[i].isalnum() or name[i] == "_"):
            return False
    return True


class ValueParser:
    def __init__(self, parent=None):
        self.variables = {}
        self.parent = parent

    def scope(self, deep=False):
        scope = ValueParser(parent=self)
        if deep:
            for variable in self.variables.values():
                scope.assign(Variable(scope, variable.name, variable.value))
        else:
            scope.variables = self.variables.copy()
        return scope

    def assign(self, variable):
        self.variables[variable.name] = variable

    def get_variable(self, span):
        var_name = str(span.trim())
        if var_name in self.variables:
            return self.variables[var_name]

    def get_or_create_variable(self, name):
        if name in self.variables:
            return self.variables[name]
        else:
            return Variable(self, name, None)

    def redefine(self, name, value):
        self.variables[name] = Variable(self, name, value)
        return self.variables[name]

    def parse(self, span, variables=False):
        origin = span
        span = span.trim()
        s = str(span)

        i = 0
        var_name = ""
        while i < len(span):
            if not is_valid_variable_name(var_name + span[i]):
                break
            var_name += span[i]
            i += 1
        if len(var_name) > 0 and var_name in self.variables:
            var = self.variables[var_name]
            origin.pos(span.pos() + len(var.name))
            if variables:
                return var
            else:
                return var.value

        for pattern, clazz, priority in _value_parsers:
            match = re.search(pattern, s)
            if match:
                _span = span.span(*match.span())
                parsable = clazz()
                if parsable.parse(self, _span) and parsable.validate():
                    origin.pos(span.pos() + match.span()[1])
                    return parsable
        return None


# All value type parsers
_value_parsers = []


def add_value_parser(regex, clazz, priority=0):
    _value_parsers.append((regex, clazz, priority))
    _value_parsers.sort(key=lambda x: x[2], reverse=True)


__all__ = ["ValueParser", "Parsable", "add_value_parser", "is_valid_variable_name"]
