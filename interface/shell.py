import tkinter as tk

from time import sleep
from threading import Thread

from interface.support import *
from command import *


class Shell(CustomText, IOInterface):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.configure(
            bg="#004",
            fg="white",
            insertbackground="white",
            insertofftime=0,
            insertwidth=2,
            selectbackground="white",
            selectforeground="black"
        )

        self._locked = 0
        self._awaiting_inputs = []

    def _add_input_data(self, line):
        if len(self._awaiting_inputs) > 0:
            self._awaiting_inputs.pop(0)[0] = line
        else:
            self._awaiting_inputs.insert(0, [line])

    def _write_and_lock(self, string, error=False, **flags):
        start = self.index("end-1c")
        self.tk.call(self._orig, "insert", tk.END, string)
        self._locked = self.int_index(tk.END) - 1
        if error:
            end = self.index("end-1c")
            self.highlight(start, end, tag="err")

    def to_end(self):
        self.see(tk.END)

    def write(self, string, to_input=False, newline=True, **flags):
        if len(string) == 0:
            return
        text = self.get_text()
        if newline and len(text) > 0 and text[-1] != "\n":
            string = "\n" + string
        self._write_and_lock(string + ("\n" if newline else ""), **flags)
        self.mark_set(tk.INSERT, tk.END)
        self.to_end()
        if to_input:
            self._add_input_data(string)

    def exec(self, string):
        self.write(string, to_input=True)

    def flush(self):
        for data in self._awaiting_inputs:
            if data[0] is None:
                data[0] = ""

    def read(self, prompt=None, **flags):
        self._write_and_lock(prompt)
        if len(self._awaiting_inputs) > 0:
            data = self._awaiting_inputs.pop()
        else:
            data = [None]
            self._awaiting_inputs.append(data)
        while data[0] is None:
            sleep(0.001)
        return data[0]

    def clear(self):
        self._locked = 0
        self.set_text("")

    def _delete(self, start, end, **kwargs):
        if start < self._locked:
            return True

    def _insert(self, position, chars, str_pos=None, **kwargs):
        enter = False
        if "\n" in chars:
            chars = chars.replace("\n", " ")
            line = self.get_text()[self._locked:-1]
            self._locked = self.int_index(tk.END) + len(chars)
            enter = True
            if len(self._awaiting_inputs) > 0:
                self._awaiting_inputs.pop(0)[0] = line
        if position < self._locked:
            self.mark_set("insert", tk.END)
            chars = ""
        self.tk.call(self._orig, "insert", str_pos, chars)
        if enter:
            self.tk.call(self._orig, "insert", tk.END, "\n")
            self.mark_set("insert", tk.END)

        return True


__all__ = ["Shell"]
