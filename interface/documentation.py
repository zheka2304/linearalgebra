import tkinter as tk

from handle import *
from .support import *


class DocumentationWindow(tk.Tk, WindowHandle):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.events = {}
        self._child_wins = []

        self.title("Help")
        self.geometry("600x500-0+0")

        self._destroyed = False
        self.protocol("WM_DELETE_WINDOW", lambda: self._handle_quit())

        def _fill_list(box, text):
            import modules
            module_map = modules.get_instance().modules
            names = list(module_map.keys())
            names.sort()
            for name in names:
                if name[0] != "_":
                    box.insert("end", name)

            def _set_docs():
                name = box.get("active")
                if name:
                    doc = ""
                    from parsers import operator_associations
                    for op, associations in operator_associations.items():
                        if name in associations:
                            doc += "Associated operator: " + op + "\n\n"
                            break
                    for module in module_map[name]:
                        title = name.upper()
                        params = []
                        for arg in module.params:
                            params.append(arg.__name__)
                        doc += module.return_type.__name__ + " " + title + " (" + ", ".join(params) + ")\n"
                        for line in module.description.split("\n"):
                            line = line.strip()
                            if len(line) > 0:
                                doc += "  " + line + "\n"
                        doc += "\n"
                    text.configure(state="normal")
                    text.set_text(doc)
                    text.configure(state="disabled")

            box.bind("<<ListboxSelect>>", lambda *args: _set_docs())
            box.bind("<Button-1>", lambda *args: _set_docs())
            box.bind("<ButtonRelease-1>", lambda *args: _set_docs())

        self.list = tk.Listbox(self)
        self.list.grid(row=1, column=1, sticky="nsew")
        self.text = CustomText(self, bg="white")
        self.text.configure(state="disabled")
        self.text.grid(row=1, column=2, sticky="nsew")
        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=10)
        self.rowconfigure(1, weight=1)

        _fill_list(self.list, self.text)

    def destroyed(self):
        return self._destroyed

    def _handle_quit(self):
        self.destroy()
        self._destroyed = True

