import tkinter as tk

from .support import *


class CodeText(CustomText):
    pass


class CodeWidget(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent)
        self.text = CodeText(self, *args, **kwargs)
        self.vsb = tk.Scrollbar(self, orient="vertical", command=self.text.yview)
        self.text.configure(yscrollcommand=self.vsb.set)
        self.vsb.pack(side="right", fill="y")
        self.text.pack(side="left", fill="both", expand=True)

    def get_text(self):
        return self.text.get_text()

    def set_text(self, text):
        self.text.set_text(text)


__all__ = ["CodeWidget"]

