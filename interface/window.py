import os
import tkinter as tk
from tkinter import filedialog

from handle import *
from .shell import *
from .code import *
from .documentation import *

_root_window = None


class Window(tk.Tk, EnvironmentHandle):
    # methods:
    #   toggle_fullscreen
    #   ask_open_file
    #   ask_save_file
    #   get_source_code
    #   set_source_code
    #   destroy_children
    #   open_help
    #   exit
    def invoke_method(self, name, *args, **kwargs):
        if name == "toggle_fullscreen":
            return self.toggle_fullscreen()
        if name == "ask_open_file":
            return self.ask_open_file()
        if name == "ask_save_file":
            return self.ask_save_file()
        if name == "get_source_code":
            return self.get_source_code()
        if name == "set_source_code":
            return self.set_source_code(*args)
        if name == "destroy_children":
            return self.destroy_children()
        if name == "open_help":
            return self.open_help()
        if name == "exit":
            return self.exit()
        raise NotImplementedError(name)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.events = {}
        self._child_wins = []

        self.title("Linear Algebra")
        self.geometry("1000x500")
        self._fullscreen = False
        self._init_menu()

        self.code = CodeWidget(self, height=30, width=1000)
        self.code.grid(row=1, column=1, sticky=tk.NSEW)  # pack(fill=tk.BOTH, expand=1)
        self.shell = Shell(self, height=18, width=1000)
        self.shell.grid(row=2, column=1, sticky=tk.NSEW)  # pack(fill=tk.BOTH, expand=1)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)
        self.columnconfigure(1, weight=1)

        global _root_window
        _root_window = self

    def add_child_win(self, win):
        self._child_wins.append(win)

    def _init_menu(self):
        menu = tk.Menu(self)
        file = tk.Menu(menu, tearoff=0)
        file.add_command(label="Open", command=self._event("open"), accelerator="Ctrl+O")
        file.add_command(label="Save", command=self._event("save"), accelerator="Ctrl+S")
        file.add_command(label="Save As", command=self._event("save-as"), accelerator="Ctrl+Shift+S")
        file.add_separator()
        file.add_command(label="Exit", command=self._event("exit"))
        menu.add_cascade(label="File", menu=file)
        shell = tk.Menu(menu, tearoff=0)
        shell.add_command(label="Toggle Fullscreen", command=self._event("fullscreen"), accelerator="Ctrl+F")
        shell.add_separator()
        shell.add_command(label="Clear", command=self._event("clear"))
        shell.add_command(label="Clear Environment", command=self._event("clear-env"))
        menu.add_cascade(label="Shell", menu=shell)
        run = tk.Menu(menu, tearoff=0)
        run.add_command(label="Run", command=self._event("run"), accelerator="Ctrl+R")
        run.add_command(label="Run and Verbose", command=self._event("run-verbose"), accelerator="Ctrl+Shift+R")
        run.add_separator()
        run.add_command(label="Terminate", command=self._event("terminate"), accelerator="Ctrl+B")
        menu.add_cascade(label="Run", menu=run)
        help_menu = tk.Menu(menu, tearoff=0)
        help_menu.add_command(label="Modules", command=self._event("help"), accelerator="F1")
        menu.add_cascade(label="Help", menu=help_menu)
        self.config(menu=menu)

        self.bind('<Control-o>', self._event("open"))
        self.bind('<Control-s>', self._event("save"))
        self.bind('<Control-r>', self._event("run"))
        self.bind('<Control-b>', self._event("terminate"))
        self.bind('<Control-Shift-R>', self._event("run-verbose"))
        self.bind('<Control-f>', self._event("fullscreen"))
        self.bind('<Control-Shift-S>', self._event("save-as"))
        self.bind('<F1>', self._event("help"))

    def toggle_fullscreen(self):
        self._fullscreen = not self._fullscreen
        self.shell.configure(height=500 if self._fullscreen else 18)
        self.attributes('-fullscreen', self._fullscreen)
        self.shell.to_end()

    def open_help(self):
        return DocumentationWindow()

    def get_source_code(self):
        return self.code.get_text()

    def set_source_code(self, src):
        self.code.set_text(src)

    def add_event(self, name, callback, **kwargs):
        if name not in self.events:
            self.events[name] = []
        self.events[name].append((callback, kwargs))

    def _event(self, name):
        def caller(*args, **kwargs):
            if name in self.events:
                for callback, kw in self.events[name]:
                    callback(self, **kw)
        return caller

    def destroy_children(self):
        for win in self._child_wins:
            win.destroy()
        self._child_wins = []

    def exit(self):
        self.destroy_children()
        self.quit()

    def get_shell(self):
        return self.shell

    def ask_open_file(self):
        return filedialog.askopenfilename(initialdir=os.getcwd())

    def ask_save_file(self):
        return filedialog.asksaveasfilename(initialdir=os.getcwd())


def get_root_window():
    return _root_window


__all__ = ["Window", "get_root_window"]
