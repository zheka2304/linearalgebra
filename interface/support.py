import tkinter as tk


class CustomText(tk.Text):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # create a proxy for the underlying widget
        self._orig = self._w + "_orig"
        self.tk.call("rename", self._w, self._orig)
        self.tk.createcommand(self._w, self._proxy)

        self.tag_config('err',
                        background='#ff0000',
                        selectbackground="yellow",
                        selectforeground="blue",
                        )

    def get_text(self):
        return self.get("1.0", tk.END)

    def highlight(self, start, end, tag=None):
        self.tag_add(tag, start, end)

    def set_text(self, text):
        self.delete("1.0", tk.END)
        self.insert("1.0", text)

    def int_index(self, pos):
        result = self.tk.call(self, "count", "1.0", pos)
        if result == "":
            return 0
        return int(result)

    def _insert(self, position, chars, **kwargs):
        return False

    def _delete(self, start, end, **kwargs):
        return False

    def _replace(self, position):
        pass

    def _get_del_span(self, *args):
        if len(args) == 1:
            start = self.int_index(args[0])
            end = start + 1
        else:
            start = self.int_index(args[0])
            end = self.int_index(args[1])
        return start, end

    def _proxy(self, command, *args):
        prevent = False
        if command in ("insert", "delete"):
            pos = self.index(tk.INSERT)
            if command == "insert":
                prevent = self._insert(self.int_index(pos), *args[1:], str_pos=pos)
            if command == "delete":
                prevent = self._delete(*self._get_del_span(*args), str_span=args)
        result = ""
        if not prevent:
            try:
                result = self.tk.call((self._orig, command) + args)
            except tk.TclError:
                pass
        return result


__all__ = ["CustomText"]
