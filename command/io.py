

class IOInterface:
    # Writes string
    def write(self, string, **flags):
        raise NotImplementedError()

    # Reads one line
    def read(self, prompt=None, **flags):
        raise NotImplementedError()

    # Flushes all inputs
    def flush(self):
        raise NotImplementedError()

    # Write to input stream
    def exec(self, **flags):
        raise NotImplementedError()

    # Clear the screen
    def clear(self):
        raise NotImplementedError()


class ConsoleIO(IOInterface):
    def write(self, string, **flags):
        print(string)

    def read(self, prompt=None, **flags):
        return input(prompt)

    def exec(self, **flags):
        pass

    def flush(self):
        pass

    def clear(self):
        pass
