import re
import traceback


class CommandException(Exception):
    pass


class CommandInterface:
    def __init__(self):
        self.bindings = {}
        self._exit = False
        self.io = None

        self.bind("exit", lambda *args, **kwargs: self.exit())
        self.bind("#exit", lambda *args, **kwargs: None)

    def _invoke(self, caller, *args, no_split=False, **kwargs):
        if no_split:
            args = (" ".join(args), )
        return caller(*args, **kwargs)

    # noinspection PyBroadException
    def call(self, binding, *args, io=None):
        if binding in self.bindings:
            caller, kwargs = self.bindings[binding]
            try:
                self._invoke(caller, *args, io=io, **kwargs)
            except Exception:
                raise CommandException(traceback.format_exc())
        else:
            raise CommandException("no binding " + str(binding))

    def bind(self, name, caller, **kwargs):
        self.bindings[name] = (caller, kwargs)

    def interpret(self, command, io=None):
        command = command.strip()
        if len(command) == 0:
            return
        command = command + " "
        if re.fullmatch("^\\b\w+\\b[\s]+.*$", command):
            start, end = re.match("^\\b\w+\\b", command).span()
            binding = command[start:end]
            if binding in self.bindings or "#expression" not in self.bindings:
                args = []
                for arg in re.split("\s+", command[end:]):
                    if len(arg) > 0:
                        args.append(arg)
                self.call(binding, *tuple(args), io=io)
                return
        self.call("#expression", command, io=io)

    def exit(self):
        self._exit = True
        self.call("#exit")

    def exec(self, cmd):
        if self.io is not None:
            self.io.exec(cmd)
        else:
            self.interpret(cmd)

    def mainloop(self, io):
        self._exit = False
        self.io = io
        while not self._exit:
            try:
                self.interpret(io.read(prompt=">>> "), io)
            except CommandException as e:
                io.write(str(e), error=True)
        self.io = None

