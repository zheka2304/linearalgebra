from structures import *
from parsers import *

import sys


all_unit_tests = [
    # modules
    # write here expression with your modules
    ("sub_ndn_n(50, 12, 4)", NNumber(2)),
    ("mul_nn_n(132, 13)", NNumber(1716)),
    ("mul_nd_n(139, 3)", NNumber(417)),
    ("div_nn_dk(567, 12)", NNumber(4)),
    ("div_nn_n(198, 14)", NNumber(14)),

    # equality
    ("1 == 2", Boolean(False)),
    ("112 == 112", Boolean(True)),
    ("1 == -1", Boolean(False)),
    ("121 == -100", Boolean(False)),
    ("1/2 > 1/3", Boolean(True)),
    ("1/2 < 1/3", Boolean(False)),

    # addition
    ("1 + 2", Number(3)),
    ("1211273 + 16494271", Number(17705544)),

    # complex syntax
    ("(1 == 2) != (2 + 2 == 4)", Boolean(True)),

    # variables
    ("a := 1 + 2 + 3", Number(6)),
    ("b := a + 1", Number(7)),
    ("_1d2:=a +  b", Number(13)),
    ("_666:= _1d2 == a + b", Boolean(True)),
    ("_res2 := (res:= (_666 --> False))", Boolean(False)),
    ("_1d3 := (_1d2:= _1d2 + a + b) + 1", Number(27)),

    ("mul_zz_z(-2, 2)", Number(-4)),
    ("mul_zz_z(-2, 3)", Number(-6)),
    ("mul_zz_z(-2, -3)", Number(6)),
    ("mul_zz_z(2, 3)", Number(6)),

    ("div_zz_z(-4, 2)", Number(-2)),
    ("div_zz_z(-3, 2)", Number(-2)),
    ("div_zz_z(4, 2)", Number(2)),
    ("div_zz_z(3, 2)", Number(1)),
    ("div_zz_z(-10, 3)", Number(-4)),
    ("div_zz_z(-9, 3)", Number(-3)),

    ("mod_zz_z(10, 3)", Number(1)),
    ("mod_zz_z(-10, 3)", Number(2)),
    ("mod_zz_z(-9, 3)", Number(0)),
    ("mod_zz_z(9, 3)", Number(0)),

    ("gcf_nn_n(2, 3)", Number(1)),
    ("gcf_nn_n(123, 369)", Number(123)),
    ("gcf_nn_n(0, 23)", Number(23)),
    ("gcf_nn_n(23, 0)", Number(23)),

    ("red_q_q(1/2)", Fraction(numerator=1, denominator=2, negative=False)),
    ("red_q_q(2/4)", Fraction(numerator=1, denominator=2, negative=False)),
    ("red_q_q(-2/4)", Fraction(numerator=1, denominator=2, negative=True)),
    ("red_q_q(-1/2)", Fraction(numerator=1, denominator=2, negative=True)),
    ("red_q_q(-123/369)", Fraction(numerator=1, denominator=3, negative=True)),
    ("red_q_q(-2/100)", Fraction(numerator=1, denominator=50, negative=True)),

    ("trans_q_z(3/1)", Number(3)),
    ("trans_q_z(-5/1)", Number(-5)),
    ("trans_z_q(3)", Fraction(numerator=3, denominator=1, negative=False)),
    ("trans_z_q(-5)", Fraction(numerator=5, denominator=1, negative=True)),

    ("mul_qq_q(2/4, 2/3)", Fraction(numerator=1, denominator=3, negative=False)),
    ("mul_qq_q(-2/4, 2/3)", Fraction(numerator=1, denominator=3, negative=True)),
    ("mul_qq_q(2/4, -2/3)", Fraction(numerator=1, denominator=3, negative=True)),
    ("mul_qq_q(0/1, -2/3)", Fraction(numerator=0)),
    ("mul_qq_q(-2/4, 0/1)", Fraction(numerator=0)),

    ("div_qq_q(2/4, 2/3)", Fraction(numerator=3, denominator=4, negative=False)),
    ("div_qq_q(-2/4, 2/3)", Fraction(numerator=3, denominator=4, negative=True)),
    ("div_qq_q(2/4, -2/3)", Fraction(numerator=3, denominator=4, negative=True)),
    ("div_qq_q(0/1, -2/3)", Fraction(numerator=0)),

    ("add_zz_z(4, 6)", Number(10)),
    ("add_zz_z(-4, 6)", Number(2)),
    ("add_zz_z(4, -6)", Number(-2)),
    ("add_zz_z(6, -4)", Number(2)),
    ("add_zz_z(-6, 4)", Number(-2)),

    ("lcm_nn_n(1, 2)", NNumber(2)),
    ("lcm_nn_n(23, 14)", NNumber(322)),
    ("lcm_nn_n(14, 23)", NNumber(322)),

    ("sub_qq_q(1/2, 1/2)", Fraction(numerator=0)),
    ("sub_qq_q(1/2, 1/3)", Fraction(numerator=1, denominator=6)),
    ("sub_qq_q(-1/2, -1/3)", Fraction(numerator=1, denominator=6, negative=True)),
    ("sub_qq_q(-1/2, 1/3)", Fraction(numerator=5, denominator=6, negative=True)),
    ("sub_qq_q(1/2, -1/3)", Fraction(numerator=5, denominator=6)),

    ("[[1,2]] == [[1,2]]", Boolean(True)),
    ("[[1/2,2]] == [[1,2]]", Boolean(False)),
    ("[[1,2]] != [[1,2,3]]", Boolean(True)),
    ("[[1, -2]] == [[1,2]]", Boolean(False)),

    ("sub_pp_p([[1 2]], [[0 1]])", Polynomial([1,1])),
    ("sub_pp_p([[1 2]], [[3 3]])", Polynomial([-2,-1])),
    ("sub_pp_p([[1 2]], [[0 0 0 3]])", Polynomial([1, 2, 0, -3])),

    ("mul_pq_p([[1 0 0 2/3]], 3/2)", Polynomial([Fraction(numerator=3,denominator=2), 0, 0, 1])),
    ("mul_pq_p([[1 0 0 2/3]], -3/2)", Polynomial([Fraction(numerator=-3,denominator=2), 0, 0, -1])),

    #("add_pp_p([[1 2]], [[2 2]])", Polynomial(factors=[3,4])),
]


def run_unit_tests(modules, tests=None):
    if tests is None:
        tests = all_unit_tests

    passed = 0
    parser = ExpressionParser(modules, ValueParser())
    interpreter = StackInterpreter(modules)
    for string, result in tests:
        try:
            exp = parser.parse(ParserSpan(string))
            res = interpreter.interpret(exp)
            if res != result:
                print("expression unit test '" + string + "' failed: " +
                      "\n\tinvalid result " + str(res) + ":" + type(res).__name__ + " expected " + str(result) + ":" + type(result).__name__ +
                      "\n\tdisassembled = '" + str(exp) + "'" +
                      "\n\tparsed = '" + exp.parsed_to_string() + "'")
            else:
                passed += 1
        except ParserException as e:
            print("expression unit test '" + string + "' failed with exception: " + str(e))

    print("successfully passed " + str(passed) + "/" + str(len(tests)) + " unit tests", file=(None if passed == len(tests) else sys.stderr))
