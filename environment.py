from modules import registry as module_registry

from parsers import *
from interface import *
from command import *
from tests import *

from threading import *


class ExecutionEnvironment:
    def __init__(self):
        # Initialize all variables
        self.modules = module_registry.get_instance()
        self.values = ValueParser()
        self.parser = ExpressionParser(self.modules, self.values)
        self.interpreter = StackInterpreter(self.modules)

    def interpret_expression(self, exp):
        return self.interpreter.interpret(self.parser.parse(ParserSpan(exp)))

    def get_program_parser(self):
        return ProgramParser(self.interpreter, self.values, self.parser)


def compile_source(source, env=None):
    if env is None:
        env = ExecutionEnvironment()
    return env.get_program_parser().parse(ParserSpan(str(source)))


#
#


def command_expression(exp, env=None, io=None, **kwargs):
    try:
        module_registry.get_instance().reset_options()
        result = env.shell_env.interpret_expression(exp)
        io.write("result: " + str(result))
    except ParserException as e:
        io.write("error: " + str(e), error=True)


def command_interpret(source, io=None, env=None, **kwargs):
    env._program = None
    try:
        env._program = compile_source(ParserSpan(source))
        env._program.interpret(io=io)
    except ParserException as e:
        io.write("failed to parse: " + str(e), error=True)
    except InterpretationException as e:
        io.write("failed to interpret: " + str(e), error=True)
        if e.statement is not None:
            io.write("Statement:\n  " + repr(e.statement))
        if env._program is not None:
            io.write(repr(env._program))


def command_clear(*args, env=None, io=None, **kwargs):
    if "-e" in args or "-env" in args:
        env.clear_shell_env()
    io.clear()


class Environment:
    def __init__(self):
        self.commands = CommandInterface()
        self.shell_env = ExecutionEnvironment()
        self.window = None
        self._help_window = None
        self._program = None
        self._exit = False
        self._file_path = None
        self._temp = ".temp"

        # execution
        def _run(*args, io=None, **kwargs):
            if len(args) == 0 or len(args[-1]) == 0 or args[-1][0] == "-":
                path = self._temp
                self._save_temp()
            else:
                path = args[-1]
            with open(path, "r") as f:
                if f is not None:
                    src = f.read()
                    f.close()
                    self._program = None
                    try:
                        self._program = compile_source(src)
                        io.write("")
                        if "-v" in args:
                            io.write(repr(self._program))
                        if "-p" in args:
                            io.read("press any key to start...")
                        self._program.interpret(io=io)
                    except ParserException as e:
                        io.write("failed to parse: " + str(e), error=True)
                    except InterpretationException as e:
                        io.write("failed to interpret: " + str(e), error=True)
                        if e.statement is not None:
                            io.write("Statement:\n" + e.get_stack(prefix="  "), error=True)
                else:
                    io.write("failed to open: " + str(path), error=True)

        self.commands.bind("run", _run)
        # expression
        self.commands.bind("#expression", command_expression, env=self)
        self.commands.bind("interpret", command_interpret, env=self, no_split=True)
        # util commands
        self.commands.bind("clear", command_clear, env=self)
        self.commands.bind("exit", lambda *args, env=None, **kwargs: env.exit(), env=self)
        self.commands.bind("echo", lambda s, io=None, **kwargs: io.write(s), no_split=True)
        self.commands.bind("fullscreen", lambda *args, env=None, **kwargs: env.toggle_fullscreen(), env=self)

    def terminate_current_program(self):
        if self.window is not None:
            self.window.invoke_method("destroy_children")
        if self._program is not None:
            self._program.terminate()
            self._program = None
            self.commands.exec(" ")

    def toggle_fullscreen(self):
        if self.window is not None:
            self.window.invoke_method("toggle_fullscreen")

    def open_help(self):
        if self._help_window is None or self._help_window.destroyed():
            self._help_window = self.window.invoke_method("open_help")

    def _save_temp(self):
        if self.window is not None:
            try:
                with open(self._temp, "w") as f:
                    if f is not None:
                        f.write(self.window.invoke_method("get_source_code"))
                        f.close()
            except FileNotFoundError:
                pass

    def _read_temp(self):
        try:
            with open(self._temp, "r") as f:
                if f is not None:
                    text = f.read()
                    f.close()
                    i = 1
                    while len(text) >= i and text[-i] == "\n":
                        i += 1
                    if i > 1:
                        text = text[:-i + 1]
                    return text
        except FileNotFoundError:
            pass

    def _get_and_save_source(self):
        self._save_temp()
        return self._read_temp()

    def attach_handle(self, win):
        def cmd(command, terminate=False):
            def callback(win, **kwargs):
                if terminate:
                    self.terminate_current_program()
                self.commands.exec(command)
            return callback
        self.window = win
        self.window.add_event("exit", cmd("exit", terminate=True))
        
        self.window.add_event("fullscreen", lambda *args, **kwargs: self.toggle_fullscreen())
        self.window.add_event("clear", cmd("clear"))
        self.window.add_event("clear-env", cmd("clear -env"))

        self.window.add_event("run", cmd("run", terminate=True))
        self.window.add_event("run-verbose", cmd("run -v -p", terminate=True))
        self.window.add_event("terminate", lambda *args, **kwargs: self.terminate_current_program())
        self.window.add_event("help", lambda *args, **kwargs: self.open_help())

        def _open(win, **kwargs):
            path = self.window.invoke_method("ask_open_file")
            if path is not None and len(path) > 0:
                self._file_path = path
                with open(path, "r") as f:
                    win.set_source_code(f.read())
                    f.close()

        def _save_as(win, path=None, **kwargs):
            source = win.get_source_code()
            if len(source) > 0:
                if path is None:
                    path = self.window.invoke_method("ask_save_file")
                if path is not None and len(path) > 0:
                    self._file_path = path
                    with open(path, "w") as f:
                        f.write(source)
                        f.close()

        def _save(win, **kwargs):
            _save_as(win, path=self._file_path)

        self.window.add_event("open", _open)
        self.window.add_event("save", _save)
        self.window.add_event("save-as", _save_as)

        _temp_source = self._read_temp()
        if _temp_source:
            self.window.invoke_method("set_source_code", _temp_source)

    def clear_shell_env(self):
        self.shell_env = ExecutionEnvironment()

    def _exit_internal(self):
        if not self._exit:
            self._exit = True
            self.commands.exit()

    def exit(self):
        self._save_temp()
        self._exit_internal()
        if self.window is not None:
            self.window.invoke_method("exit")

    def _terminate_and_exit(self):
        self.terminate_current_program()
        self.commands.exec("exit")

    def mainloop(self):
        io = ConsoleIO()
        if self.window:
            io = self.window.get_shell()
            self.window.protocol("WM_DELETE_WINDOW", lambda: self._terminate_and_exit())

        def run():
            self.commands.mainloop(io=io)
        Thread(target=run).start()
        Thread(target=lambda: run_unit_tests(self.shell_env.modules)).start()
        if self.window:
            self.window.mainloop()


if __name__ == "__main__":
    ENV = Environment()
    ENV.attach_window(Window())
    ENV.mainloop()

__all__ = ["Environment"]
