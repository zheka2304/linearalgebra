from parsers import *
from modules import *


class Number(Parsable):
    def __init__(self, val=None, digits=None, negative=False):
        self.negative = False
        self.digits = [0]

        if isinstance(val, Number):
            self.digits = val.digits[:]
            self.negative = val.negative
        elif val is not None:
            if val < 0:
                val = -val
                self.negative = True
            self.digits = []
            while val > 0:
                self.digits.append(val % 10)
                val //= 10
        elif digits is not None:
            self.digits = digits
            self.negative = negative
        self.validate()

    def parse(self, parser, span):
        if span[0] == "-":
            span = span.span(1)
            self.negative = True
        self.digits = []
        for digit in span:
            if not digit.isdecimal():
                return False
            self.digits.insert(0, int(digit))
        return True

    def validate(self):
        if len(self.digits) == 0:
            self.digits.append(0)

        while len(self.digits) > 1 and self.digits[-1] == 0:
            self.digits.pop()

        if self.is_zero():
            self.negative = False

        for digit in self.digits:
            if digit < 0 or digit > 9 or not isinstance(digit, int):
                return False
        return True

    def __copy__(self):
        return Number(val=self)

    def __str__(self):
        result = ""
        for digit in self.digits:
            result = str(digit) + result
        if self.negative:
            result = "-" + result
        return result

    def __len__(self):
        if self.is_zero():
            return 0
        return len(self.digits)

    def __getitem__(self, key):
        if key >= len(self.digits):
            return 0
        return self.digits[key]

    def __setitem__(self, key, val):
        while len(self.digits) <= key:
            self.digits.append(0)
        self.digits[key] = val

    def __iter__(self):
        return self.digits.__iter__()

    def is_zero(self):
        for digit in self.digits:
            if digit != 0:
                return False
        return True

    def opposite(self):
        if self.negative:
            return NNumber(digits=self.digits, negative=False)
        else:
            return Number(digits=self.digits, negative=True)

    def __lt__(self, other):
        if isinstance(other, Number):
            return bool(call_module("com_zz_lt", self, other))
        elif isinstance(other, int):
            return self < Number(other)
        else:
            raise TypeError()

    def __eq__(self, other):
        if isinstance(other, Number):
            return bool(call_module("com_zz_eq", self, other))
        elif isinstance(other, int):
            return self == Number(other)
        else:
            return False

    def __le__(self, other):
        return self < other or self == other

    def __ne__(self, other):
        return not self == other

    def __gt__(self, other):
        if isinstance(other, int) or isinstance(other, Number):
            return not (self <= Number(other))
        else:
            raise TypeError()

    def __ge__(self, other):
        if isinstance(other, int) or isinstance(other, Number):
            return not (self < Number(other))
        else:
            raise TypeError()

    def __add__(self, other):
        if isinstance(other, Number):
            return call_module("add_zz_z", self, other)
        elif isinstance(other, int):
            return call_module("add_zz_z", self, Number(other))
        else:
            raise TypeError()

    def __sub__(self, other):
        if isinstance(other, Number):
            return call_module("sub_zz_z", self, other)
        elif isinstance(other, int):
            return call_module("sub_zz_z", self, Number(other))
        else:
            raise TypeError()

    def __mul__(self, other):
        if isinstance(other, Number):
            return call_module("mul_zz_z", self, other)
        elif isinstance(other, int):
            return call_module("mul_zz_z", self, Number(other))
        else:
            raise TypeError()

    def __floordiv__(self, other):
        if isinstance(other, Number):
            return call_module("div_zz_z", self, other)
        elif isinstance(other, int):
            return call_module("div_zz_z", self, Number(other))
        else:
            raise TypeError()

    def __mod__(self, other):
        if isinstance(other, Number):
            return call_module("mod_zz_z", self, other)
        elif isinstance(other, int):
            return call_module("mod_zz_z", self, Number(other))
        else:
            raise TypeError()

    def __pow__(self, other):
        result = Number(1)
        i = 0
        while i < other:
            result *= self
            i += 1
        return result

    def __radd__(self, other):
        if isinstance(other, Number) or isinstance(other, int):
            return self + other
        else:
            raise TypeError()

    def __rsub__(self, other):
        if isinstance(other, Number) or isinstance(other, int):
            return Number(other) - self
        else:
            raise TypeError()

    def __rmul__(self, other):
        if isinstance(other, Number) or isinstance(other, int):
            return self*other
        else:
            raise TypeError()

    def __rfloordiv__(self, other):
        if isinstance(other, Number) or isinstance(other, int):
            return Number(other)//self
        else:
            raise TypeError()

    def __rmod__(self, other):
        if isinstance(other, Number) or isinstance(other, int):
            return Number(other)%self
        else:
            raise TypeError()

    def __rpow__(self, other):
        if isinstance(other, Number) or isinstance(other, int):
            return Number(other)**self
        else:
            raise TypeError()

    def __neg__(self):
        return self.opposite()

    def __abs__(self):
        if self.negative:
            return self.opposite()
        else:
            return self

    def __int__(self):
        return int(str(self))


class NNumber(Number):
    def validate(self):
        if super().validate():
            return not self.negative
        return False

    def __copy__(self):
        return NNumber(val=self)


# Register
add_value_parser("^[-+]?[0-9]+", NNumber, priority=1)
add_value_parser("^[-+]?[0-9]+", Number, priority=0)
__all__ = ["Number", "NNumber"]
