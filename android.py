__version__ = "1.0.0"


import kivy

from kivy.app import App
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.uix.textinput import TextInput

from time import sleep

from command import IOInterface
from handle import EnvironmentHandle
from environment import Environment


Window.size = (1920, 1080)


class ShellOutputWidget(TextInput):
    shell = None

    def insert_text(self, substring, from_undo=False):
        pass

    def delete_selection(self, from_undo=False):
        pass

    def do_backspace(self, from_undo=False, mode="bkspc"):
        pass

    def write(self, string, newline=True, **kwargs):
        if newline and (len(string) == 0 or string[-1] != "\n"):
            string += "\n"
        self.text += string

    def clear(self):
        self.text = ""


class ShellInputWidget(TextInput):
    shell = None
    inputs = []

    def insert_text(self, substring, from_undo=False):
        if substring == "\n":
            text = self.text
            self.text = ""
            self.command(text)
        else:
            super(ShellInputWidget, self).insert_text(substring, from_undo=from_undo)

    def command(self, cmd):
        self.add_input(cmd, instant=True)

    def read(self, prompt, **kwargs):
        while len(self.inputs) == 0:
            sleep(0.001)
        return self.inputs.pop(0)

    def add_input(self, string, instant=True):
        if not instant or len(self.inputs) == 0:
            self.inputs.append(string.strip())


class AndroidShell(IOInterface):
    def __init__(self, shell_input, shell_output):
        self.input = shell_input
        self.input.shell = self
        self.output = shell_output
        self.output.shell = self

    def write(self, string, **flags):
        return self.output.write(string, **flags)

    def read(self, prompt=None, **flags):
        return self.input.read(prompt, **flags)

    def flush(self):
        pass

    def exec(self, string, **flags):
        self.output.write(string)
        self.input.add_input(string, instant=False)

    def clear(self):
        self.output.clear()


class AndroidEnvironmentHandle(EnvironmentHandle):
    def __init__(self, shell):
        self.shell = shell
        self.source = ""
        self.events = {}

    # events:
    #   clear
    #   clear-env
    #   run
    #   run-verbose
    #   terminate
    #   help
    #   open
    #   save
    #   save-as
    #   exit
    def add_event(self, name, callback, **kwargs):
        if name not in self.events:
            self.events[name] = []
        self.events[name].append((callback, kwargs))

    def invoke_event(self, name, **kwargs):
        if name in self.events:
            for callback, kw in self.events[name]:
                callback(self, **kw, **kwargs)

    # methods:
    #   toggle_fullscreen
    #   ask_open_file
    #   ask_save_file
    #   get_source_code
    #   set_source_code
    #   destroy_children
    #   open_help
    #   exit
    def invoke_method(self, name, *args, **kwargs):
        if name == "get_source_code":
            return self.source
        if name == "set_source_code":
            self.source = args[0]
        if name == "ask_open_file":
            return None
        # other methods cannot be applied for android version

    def get_shell(self):
        return self.shell

    def protocol(self, name, callback):
        pass

    def mainloop(self):
        pass


handle = None
environment = None


def init_android_env(shell_input, shell_output):
    global handle, environment
    handle = AndroidEnvironmentHandle(AndroidShell(shell_input, shell_output))
    environment = Environment()
    environment.attach_handle(handle)
    environment.mainloop()


class MyApp(App):
    def start(self):
        pass

    def build(self):
        self.root = Builder.load_file('main_form.kv')
        self.root.ids.shell_input.text = "changed!"
        init_android_env(self.root.ids.shell_input, self.root.ids.shell_output)
        return self.root


if __name__ == '__main__':
    MyApp().run()

